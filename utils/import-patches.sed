#!/usr/bin/sed -f
#
# use this sed script to import patches back from repo
# where mkit has been embedded
#
#  1. in the downstream repo, get patches using `git format-patch`,
#  2. apply this script to all files generated,
#  3. use `git am` to import patches back
#

/^\(--- a\|+++ b\|diff\)/ s:utils/mkit/include:src/include:g
/^\(--- a\|+++ b\|diff\)/ s:utils/mkit/make:src/make:g
/^\(--- a\|+++ b\|diff\)/ s:utils/mkit/stub:src/stub:g
/^\(--- a\|+++ b\|diff\)/ s:utils/mkit/mkit.mk:src/mkit.mk:g
/^\(--- a\|+++ b\|diff\)/ s:src/include/vars.sh:src/include/vars.sh.skel:g
/^index /           s:100755$:100644:
/^.MKIT_VERSION=/   s:MKIT_VERSION=.*$:MKIT_VERSION=__MKIT_PROJ_VERSION__:
