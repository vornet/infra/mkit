MKit
====

Simple Makefile target helper.

MKit can help you with boring but important tasks such as:

 *  starting up your project under git with sane structure,

 *  building and installing your project,

 *  managing release history and tags,

 *  ensuring reliable version information is always included
    with any package (from scratch builds to official releases),

 *  writing changelogs,

 *  packaging for distributions (or just spitting out tarballs).


Goals
-----

 *  Be distro-agnostic -- you can use it to prepare RPM spec files,
    debian meta-data or just tarballs.

 *  Be very simple to start up, especially for smaller non-arch-specific
    projects such as helper scripts and distributable config files.

 *  Encourage best development practices such as semantic versioning.

 *  Make file deployment easy: no need to write install script;
    just define roots and relative file paths.


Non-goals
---------

 *  Manage huge and complicated tree of build targets.

    If you want to really build "serious" "big" stuff from hundreds of
    source files, MKit is probably not for you.  Nobody has ever tried
    to use it that way.  What MKit understands as "building" is just
    replacing macros in files.  (Some extensibility will  be added in
    future but scaling to huge things is not a priority for now.)

 *  Become a complete solution for building all your RPMs and DEBs.

    Other tools such as Copr already exist for this purpose and do
    it well.  Also, building is best done in isolated environment, while
    MKit is really just about managing your repo.


Your first project with MKit
----------------------------

MKit is intended to be *embedded* inside your git repo.  We will
demonstrate simple usage on a demo project `myproj` that delivers one
script and one config file:


### 1. start your git repo ###

    $ mkdir myproj
    $ cd myproj
    $ git init
    $ mkdir src
    $ echo 'echo Hello world' > src/hello
    $ echo 'some config'      > src/hello.conf
    $ git add .
    $ git commit -m "Initial commit"
    $ git tag v0.0.0

Note that a valid version tag like *v0.0.0* is necessary to help `vbump_*`
targets compose template change overview message. (Future versions of MKit
should probably learn to improvise a bit and get rid of this requirement
though.)


### 2. embed MKit into your repo ###

    $ git clone <url-to-mkit>
    $ cd mkit
    $ make install DESTDIR=<path/to/myproj>
    $ echo 'MKIT_DIR=utils/mkit'          >> Makefile
    $ echo 'include $(MKIT_DIR)/mkit.mk'  >> Makefile
    $ git add .
    $ git commit -m "Add MKit v0.0.35"

Using Makefile is not mandatory; you can call MKit commands directly
via *utils/mkit/make* command.  It is conventional, however: your users
will be able to use the ubiquitous: `make` and `sudo make install`,
and you get tab completion for free.


### 3. add mkit.ini with everything about your project ###

Create file called 'mkit.ini' in the root of your repo:

    [project]
        version     = 0.0.0
        name        = myproj
        pkgname     = bmo
        relsrc      = master

    [dist]
        tarball = Makefile
        tarball = mkit.ini
        tarball = src
        tarball = utils

    [ENV]
        PREFIX = /usr/local

    [roots]
        bin     = [ENV:PREFIX]/bin
        share   = [ENV:PREFIX]/share/myproj

    [modes]
        bin     = 755
        share   = 644

    [files]
        bin     = src/hello
        share   = src/hello.conf

    #mkit version=0.0.35

This is pretty minimal but working project. (You can ignore the `#mkit
version` line; it just denotes version of 'mkit.ini' file format).

    $ git add mkit.ini
    $ git commit -m "Add mkit.ini"


### 4. Prepare your first release ###

We have decided to make just minor Z-stream release, i.e. 0.0.1.
First, we need to raise the version:

    $ make vbump_z

This will open your editor with commit message already prepared for
a "Bump version" commit.  Ths is a special type of commit that accompanies
every release and its purpose is to track the version change itself and
provide human-readable overview of changes since the last release.

Next step is doing actual release; that is, generate an annotated tag
and update stable branch (we haven't defined that in our demo).

    $ make release_z

Note that `release_z`, makes "Bump version" commits mandatory; this is
to help you keep your release history clean and documented.


### 5. Release it out into the wild ###

At this point, what you probably want to do is push the release to your
main repo:

    $ git push
    $ git push --tags

So new version is out.  What's left is to publish the tarballs, e.g.:

    $ make dist
    $ scp myproj-0.0.1.tar.gz your:web/server

Note that tarball is now independent and can be installed on any typical
Linux machine using common workflow:

    $ tar -xvzf myproj-0.0.1.tar.gz
    $ cd myproj-0.0.1
    $ make
    $ make install      # can add DESTDIR=/some/test/path


### Further steps ###

The tutorial above has been cut down to really only show the most common
features.  What you might want to do next:

 *  Add SPEC file and debian dir templates (adding `make rpmstuff` and
    `make debstuff` under your belt).  You can use `utils/mkit/stub` script
    for that.

 *  Add macros inside some of your scripts to include stuff known
    only at install time (versions, paths...) so that your application
    can report --version as accurately as possible.

 *  Do some actual development (branch off, do scratch builds, experiment)
    and see how MKit handles versioning.

MKit uses SemVer so version reflected in tarbal naming, packaging
files and macros contains enough data to track it back to actual commit.
This way you can safely distribute packages immediately to your testers
and/or CI systems.
